package com.sapibirulucu.ninedigital.main;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author Suherman 28/09/2018
 * Main Spring boot bootstrap class
 */
@Configuration
@EnableAutoConfiguration
@SpringBootApplication()
@EnableScheduling
@ComponentScan(basePackages = {"com.sapibirulucu.ninedigital"})
public class Bootstrap extends SpringBootServletInitializer
{
	public static void main(String[] args)
	{
		SpringApplication.run(Bootstrap.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application)
	{
		return application.sources(Bootstrap.class);
	}
}
