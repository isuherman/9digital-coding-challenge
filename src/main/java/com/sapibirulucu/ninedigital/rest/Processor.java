package com.sapibirulucu.ninedigital.rest;

import com.sapibirulucu.ninedigital.model.request.JsonPayload;
import com.sapibirulucu.ninedigital.services.JSONMapperService;
import com.sapibirulucu.ninedigital.services.JsonProcessorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Suherman 28/09/2018
 * Main Rest Controller class with the only handler for the incoming JSON string
 */
@RestController
@EnableAutoConfiguration
public class Processor
{
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private JsonProcessorService jsonProcessorService;

	@Autowired
	private JSONMapperService jsonMapperService;

	@PostMapping(value = "/", produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Object> jsonPostProcessor(@RequestBody String jsonString)
	{
		logger.info("Incoming JSON String: {}", jsonString);

		if (jsonString != null && jsonMapperService.isValidJSON(jsonString))
		{
			JsonPayload jsonPayload = jsonMapperService.toObject(JsonPayload.class, jsonString);

			if (jsonPayload != null)
			{
				return ResponseEntity.ok(jsonProcessorService.processJson(jsonPayload));
			}
		}

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(jsonProcessorService.errorMessage());
	}

    @GetMapping(value = "/", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Object> jsonGetProcessor()
    {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(jsonProcessorService.errorMessage());
    }
}
