package com.sapibirulucu.ninedigital.model.request;

public class Image
{
	private String showImage;

	public String getShowImage()
	{
		return showImage;
	}

	public void setShowImage(String showImage)
	{
		this.showImage = showImage;
	}
}
