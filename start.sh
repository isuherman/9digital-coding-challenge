#!/bin/bash
set -e

run_cmd() {
    if eval "$@"; then
        echo "Successful execution: $@"
    else
        echo "Failure execution: $@"
    fi
}

run_cmd "pkill -9 java"
nohup ./run.sh > application.log 2>&1 &
