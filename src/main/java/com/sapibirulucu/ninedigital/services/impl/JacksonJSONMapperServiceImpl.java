package com.sapibirulucu.ninedigital.services.impl;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.sapibirulucu.ninedigital.model.Constants;
import com.sapibirulucu.ninedigital.services.JSONMapperService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Service("JSONMapperService")
public class JacksonJSONMapperServiceImpl implements JSONMapperService
{
	private final ObjectMapper objectMapper = new ObjectMapper();
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@PostConstruct
	void init()
	{
		objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		objectMapper.setSerializationInclusion(Include.NON_NULL);
		objectMapper.setSerializationInclusion(Include.NON_EMPTY);
	}

	public String toJSON(Object object)
	{
		try
		{
			return objectMapper.writeValueAsString(object);
		}
		catch (Exception e)
		{
			logger.error("Failed to serialise to JSON", e);
		}

		return null;
	}

	public <T> T toObject(Class<T> type, String json)
	{
		try
		{
			return objectMapper.readValue(json, type);
		}
		catch (Exception e)
		{
			logger.error(Constants.FAILED_TO_PARSE_JSON_TO_TYPE, new Object[]{json, type.getName()});
		}

		return null;
	}

	public <T> List<T> toList(Class<T> type, String json)
	{
		try
		{
			final CollectionType javaType = objectMapper.getTypeFactory().constructCollectionType(List.class, type);
			return objectMapper.readValue(json, javaType);
		}
		catch (Exception e)
		{
			logger.error(Constants.FAILED_TO_PARSE_JSON_TO_TYPE, new Object[]{json, type.getName()});
		}

		return null;
	}

	public <T> T toObject(TypeReference<?> type, String json)
	{
		try
		{
			return objectMapper.readValue(json, type);
		}
		catch (Exception e)
		{
			logger.error(Constants.FAILED_TO_PARSE_JSON_TO_TYPE, new Object[]{json, type.toString()});
		}

		return null;
	}

	@Override
	public boolean isValidJSON(String str)
	{
		try
		{
			objectMapper.readValue(str, Map.class);
		}
		catch (IOException e)
		{
			return false;
		}

		return true;
	}


}