package com.sapibirulucu.ninedigital.services.impl;

import com.sapibirulucu.ninedigital.model.request.JsonPayload;
import com.sapibirulucu.ninedigital.model.request.Payload;
import com.sapibirulucu.ninedigital.model.response.InvalidResponse;
import com.sapibirulucu.ninedigital.model.response.Response;
import com.sapibirulucu.ninedigital.model.response.ValidResponse;
import com.sapibirulucu.ninedigital.services.JsonProcessorService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Suherman 28/09/2018
 *
 */
@Service
public class JsonProcessorServiceImpl implements JsonProcessorService
{
	@Override
	public ValidResponse processJson(JsonPayload jsonPayload)
	{
		ValidResponse validResponse = new ValidResponse();
		List<Response> responses = new ArrayList<>();
		Optional.ofNullable(jsonPayload.getPayload()).ifPresent(payloads -> payloads.stream().filter(Payload::isDrm).filter(payload -> payload.getEpisodeCount() > 0).forEach(payload ->
		{
			Response response = new Response();
			Optional.ofNullable(payload.getImage()).ifPresent(image -> response.setImage(image.getShowImage()));
			response.setSlug(payload.getSlug());
			response.setTitle(payload.getTitle());

			responses.add(response);
		}));

		validResponse.setResponse(responses);

		return validResponse;
	}

	@Override
	public InvalidResponse errorMessage()
	{
		InvalidResponse invalidResponse = new InvalidResponse();
		invalidResponse.setError("Could not decode request: JSON parsing failed");

		return invalidResponse;
	}
}
