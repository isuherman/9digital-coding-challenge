package com.sapibirulucu.ninedigital.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sapibirulucu.ninedigital.model.request.JsonPayload;
import com.sapibirulucu.ninedigital.model.response.ValidResponse;
import com.sapibirulucu.ninedigital.services.impl.JsonProcessorServiceImpl;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.*;

public class TestProcessor
{
	private final ObjectMapper objectMapper = new ObjectMapper();

	@Test
    public void testValidInput() throws IOException
	{
		JsonProcessorServiceImpl jsonProcessorService = new JsonProcessorServiceImpl();
		JsonPayload jsonPayload = toObject(readFile("src/test/resources/sample_valid_request.json"));
		String validResponseString = readFile("src/test/resources/sample_valid_response.json");
		ValidResponse validResponse = jsonProcessorService.processJson(jsonPayload);

    	assertEquals(validResponse.getResponse().size(), 7);

		validResponse.getResponse().forEach(response ->
		{
			assertNotNull(response.getImage());
			assertNotNull(response.getSlug());
			assertNotNull(response.getTitle());

			assertTrue(response.getImage().startsWith("http://"));
		});

		assertEquals(toJSON(validResponse), validResponseString);
    }

	private static String readFile(String path) throws IOException
	{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded);
	}

	@SuppressWarnings("unchecked")
	private <T> T toObject(String json) throws IOException
	{
		return objectMapper.readValue(json, (Class<T>) JsonPayload.class);
	}

	private String toJSON(Object object) throws JsonProcessingException
	{
		return objectMapper.writeValueAsString(object);
	}
}