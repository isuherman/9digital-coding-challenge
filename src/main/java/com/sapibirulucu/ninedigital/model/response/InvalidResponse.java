package com.sapibirulucu.ninedigital.model.response;

import java.util.List;

public class InvalidResponse
{
	private String error;

	public String getError()
	{
		return error;
	}

	public void setError(String error)
	{
		this.error = error;
	}
}
