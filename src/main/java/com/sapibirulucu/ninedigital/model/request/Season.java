package com.sapibirulucu.ninedigital.model.request;

public class Season
{
	private String slug;

	public String getSlug()
	{
		return slug;
	}

	public void setSlug(String slug)
	{
		this.slug = slug;
	}
}
