package com.sapibirulucu.ninedigital.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonPayload
{
	private List<Payload> payload;
	private Integer skip;
	private Integer take;
	private Integer totalRecords;

	public Integer getSkip()
	{
		return skip;
	}

	public void setSkip(Integer skip)
	{
		this.skip = skip;
	}

	public Integer getTake()
	{
		return take;
	}

	public void setTake(Integer take)
	{
		this.take = take;
	}

	public Integer getTotalRecords()
	{
		return totalRecords;
	}

	public void setTotalRecords(Integer totalRecords)
	{
		this.totalRecords = totalRecords;
	}

	public List<Payload> getPayload()
	{
		return payload;
	}

	public void setPayload(List<Payload> payload)
	{
		this.payload = payload;
	}
}
