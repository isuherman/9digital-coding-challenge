package com.sapibirulucu.ninedigital.model.response;

import java.util.List;

public class ValidResponse
{
	private List<Response> response;

    public List<Response> getResponse()
    {
        return response;
    }

    public void setResponse(List<Response> response)
    {
        this.response = response;
    }
}
