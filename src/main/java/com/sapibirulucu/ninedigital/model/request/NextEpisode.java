package com.sapibirulucu.ninedigital.model.request;

import java.util.Date;

public class NextEpisode
{
	private String channel;
	private String channelLogo;
	private Date date;
	private String html;
	private String url;

	public String getChannel()
	{
		return channel;
	}

	public void setChannel(String channel)
	{
		this.channel = channel;
	}

	public String getChannelLogo()
	{
		return channelLogo;
	}

	public void setChannelLogo(String channelLogo)
	{
		this.channelLogo = channelLogo;
	}

	public Date getDate()
	{
		return date;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public String getHtml()
	{
		return html;
	}

	public void setHtml(String html)
	{
		this.html = html;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}
}
