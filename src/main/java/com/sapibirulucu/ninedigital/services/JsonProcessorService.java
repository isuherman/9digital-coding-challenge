package com.sapibirulucu.ninedigital.services;

import com.sapibirulucu.ninedigital.model.request.JsonPayload;
import com.sapibirulucu.ninedigital.model.response.InvalidResponse;
import com.sapibirulucu.ninedigital.model.response.ValidResponse;

/**
 * @author Suherman 28/09/2018
 * Main JSON Processor service
 */
public interface JsonProcessorService
{
	/**
	 *
	 * @param jsonPayload
	 * Processor incoming JSON payload and return it as valid response
	 * @return
	 */
	ValidResponse processJson(JsonPayload jsonPayload);

	/**
	 * Return any invalid error response
	 * @return
	 */
	InvalidResponse errorMessage();
}
